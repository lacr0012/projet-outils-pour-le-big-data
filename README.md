# Projet Outils pour le Big Data


## Nom du projet : Prévisions des températures de l'année 2024

Ce projet consiste à prédire quelles seront les températures qui seront obtenues au cours de l'année 2024, à l'aide des données disponibles sur le site suivant : https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=90&id_rubrique=32. Pour ce projet, seules les données allant de janvier 2022 à décembre 2023 inclus ont été prises en considérations.


## Description du projet

Comme mentionné ci-dessus, ce projet a pour but de prédire les températures de 2024. 

Les prédictions ont été réalisées sur le logiciel SAS à l'aide d'une variable température, initialement en degré Kelvin et convertie en degré Celsius. 

Cette variable température correspond aux différentes températures relevées au sol par les 62 stations météo en métropole et outre-mer. Ces relevés sont effectués toutes les 3h à partir de minuit chaque jour et sont référencées sur le site tous les jours, dans la base mensuelle qui correspond au mois des relevés.

Dans le cadre du projet, en plus d'avoir pris des données seulement sur deux années, les données conservées pour l'études sont celles de la station 7072, station météo de Reims-Prunay, et sont celles de midi.

De cette manière, les résultats étaient plus lisibles. Bien qu'ils soient déjà bien compliqués à regarder pour un seul horaire. 


## Objectif pour le Big Data 

L'objectif était de réussir à prédire à partir d'une période sélectionnée pour plus de facilité de traitement. Cependant, il est bien évidemment possible de faire ces prédictions à partir de toutes les données référencées sur le site de Météo France et ainsi avoir une prédiction plus sûre concernant la température au sol.


## Contenu des bases de données (dossier "bdd")

Dans le dossier "bdd" situé plus haut, il y a 3 types de bases de données :

- station_7072.xlsx qui contient toutes les températures au sol en degré Celsius recencées par la station 7072 à midi du 1er janvier 2022 au 31 décembre 2023 

[base utilisée dans les codes python et SAS]

- stations.csv qui recense tous les numéros de stations et leurs noms respectifs 

[par exemple, 7072 = Reims-Prunay]

- synop.aaaamm.csv qui contient pour chaque mois de chaque année toutes les données fournies par les différentes stations toutes les 3h à partir de minuit

[base utilisée dans le code R]


## Contenu des codes (dossier "code")

Dans le dossier "code" situé en dessous du dossier "bdd", on y retrouve 3 codes :

- code_projet.R : ce code permet de créer la base de données station_7072.xlsx

- code_projet.py : ce code permet de prédire les température sur une période choisie (365 jours, soit 2024) à l'aide de l'algorithme "Prophet" (Amazon Forecast)

[la prédiction obtenue à travers ce code est mauvaise]

- code_projet.sas : ce code permet, à travers des séries temporelles, de prédire les températures pour l'année 2024 et de représenter visuellement ces prédictions 