# Librairies

import pandas as pd
from prophet import Prophet
import matplotlib.pyplot as plt

# Chargement des données

S7072 = pd.read_excel('C:/Users/lamel/Documents/Documents/Autre/Cours/Outils pour le BIG DATA/bdd/station_7072.xlsx')

# Renommer les colonnes

S7072.columns=['y','ds']

# Entraînement du modèle

model = Prophet()
model.fit(S7072)
future=model.make_future_dataframe(periods=365)
forecast = model.predict(future)
fig = model.plot(forecast)

# Visualisation des prédictions

fig, ax = plt.subplots(figsize=(12, 6))
model.plot(forecast, ax=ax)
ax.plot(S7072['ds'], S7072['y'], color='red', label='Original Series')
ax.set_xlabel('Date')
ax.set_ylabel('Température')
ax.legend()
plt.show()