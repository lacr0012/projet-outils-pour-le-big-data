/* Importation */

proc import datafile="/home/u59050032/M2/projet_series_temp/station_7072.xlsx" 
			out = S7072 
			dbms=xlsx 
			replace;
run;

/* Trajectoires */

proc sgplot data=S7072;
	series x=date y=t / markers markerattrs=(symbol=join);
	yaxis label='Température';
	xaxis label='Période';
	
 /* Proc spectra  */

proc spectra data=S7072  out=spectre p s ;
var t ;
weight parzen ;
run;

proc sgplot data=spectre;
series x=period y=s_01 / markers markerattrs=(color=black symbol=circlefilled);
yaxis label='Périodogramme';
run;

proc sgplot data=spectre;
series x=freq y=s_01 / markers markerattrs=(color=black symbol=circlefilled);
yaxis label='Densité Spectrale';
run;
	
/* Observation des graphiques et des corrélogrammes */
proc arima data=S7072;
identify var=t;
run;
quit;

/* Différenciation  */

/* Test de Dickey-Fuller*/
proc arima data=S7072;
identify var=t(1,352) stationarity=(adf=6);
run;
quit;

/* Test de Philips Perron*/
proc arima data=S7072;
identify var=t(1,352) stationarity=(pp=6);
run;
quit;

/* conclusion : d=1 et D=1 */

/* Choix de p,q, P, Q */
proc arima data=S7072;
identify var=t(1,352) nlag=24 scan  esacf minic;
run;
quit;

/* (p+d=1, q=1) */

/* Estimation des coefficients */

proc arima data=S7072;
identify var=t(1,352);
estimate p=1 q=1 plot;
run;
quit;

/* Prévisions */

proc arima data=S7072;
identify var=t(1,352);
estimate p=1 q=1 plot;
forecast lead=366 interval=day id=date out=prev_S7072;
run;
quit;

/* Vérification du modèle */
data prev2;
set prev_S7072;
debut='01jan2023'd; /*début des prévisions*/
fin='31dec2023'd ; /*fin des prévisions*/
if date lt debut or date gt fin then do;
forecast=.;
l95=.;
u95=.;
end;
run;

/* Comparaison Modèle-prévision */
proc sgplot data=prev2;
series x=date y=t / markers
markerattrs=(color=black )
lineattrs=(color=black)
legendlabel="Série originale" ;
series x=date y=forecast / markers
markerattrs=(color=red )
lineattrs=(color=red)
legendlabel="Prévision" ;
yaxis label= "Température";
run;

/* Prévision avec région de confiance */
data prev3;
set prev_S7072;
debut='01jan2024'd; /*début des prévisions*/
if date lt debut then do;
forecast=.;
l95=.;
u95=.;
end;
run;

/* Visualisation de la prévision future */
proc sgplot data=prev3;
series x=date y=t / markers
markerattrs=(color=black )
lineattrs=(color=black)
legendlabel="Série originale" ;
series x=date y=forecast / markers
markerattrs=(color=blue )
lineattrs=(color=blue)
legendlabel="Prévision" ;
series x=date y=l95 / markers
markerattrs=(color=green )
lineattrs=(color=green)
legendlabel="borne inf " ;
series x=date y=u95 / markers
markerattrs=(color=red )
lineattrs=(color=red)
legendlabel="borne sup" ;
yaxis label= "Température";
run;